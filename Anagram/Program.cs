﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anagram
{
    class Program
    {
        static void Main()
        {
            //Read anagrams.txt located inside Data folder in the project root folder.
            var filePath = AnagramHelper.GetFilePath();
            
            if (!File.Exists(filePath))
            {
                Console.WriteLine("File does not exists.");
                Console.ReadLine();
                return;
            }
            //Read each line from the file and fill a dictionary with the anagrams
            var list = AnagramHelper.ReadFromFile(filePath);
            //Before reading each anagram, order the dictionary by the longest anagram in the list.
            foreach (var item in list.OrderByDescending(r => r.Value.Length))
            {
                Console.WriteLine(item.Value);
            }
            Console.ReadLine();
        }

    }

}
