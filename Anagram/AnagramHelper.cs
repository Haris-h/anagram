﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anagram
{
    public static class AnagramHelper
    {
        public static string GetFilePath()
        {
            string currentDirectory = Directory.GetCurrentDirectory();
            string projectPath = Path.GetDirectoryName(Path.GetDirectoryName(currentDirectory));
            string filePath = Path.Combine(projectPath, "Data", "anagrams.txt");
           
            return filePath;
        }
        /// <summary>
        /// Reads each line of the text file. 
        /// </summary>
        /// <param name="filePath">Absolute file location</param>
        /// <returns>Dictionary with anagrams</returns>
        public static Dictionary<string,string> ReadFromFile(string filePath)
        {
            var result = new Dictionary<string, string>();
            // Read each line
            using (StreamReader reader = new StreamReader(filePath))
            {
                string line;

                while((line = reader.ReadLine()) != null)
                {
                    var sortedLine = SortCharacters(line);
                    string value;
                    //checks whether the sorted key is already present in the dictionary
                    //if it's in the list it will append the anagram to the value string
                    //otherwise it will add the anagram to the dictionary
                    if (result.TryGetValue(sortedLine, out value))
                    {
                        result[sortedLine] = value + " " + line;
                    }
                    else 
                    {
                        result.Add(sortedLine, line);
                    }
                }

            }

             return result;
        }
        /// <summary>
        /// Converts to char array, then sorts and returns string.
        /// </summary>
        /// <param name="input">String/Word/Line from file</param>
        /// <returns>Sorted string</returns>
        static string SortCharacters(string input)
        {
            var charArray = input.ToCharArray();
            Array.Sort(charArray);
            return new string(charArray);
        }
    }
}
